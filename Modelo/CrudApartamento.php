<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/styles.css" />
    <link rel="stylesheet" href="../css/bootstrap.min.css" />
    <title>Document</title>
</head>
<body>
<?php
	require_once('../conexion.php');
	require_once('Apartamento.php');

	class CrudApartamento{
		public function __construct(){}

		public function IngresarApartamento($apartamento){
			$Db = Db::Conectar();
			//Definir la inserción a realizar.
			$Insert = $Db->prepare('INSERT INTO apartamentos VALUES(:NApartamento,:CuartoUtil,:IdEstado,:MetrosCuadrados,:Parqueadero,:IdParqueadero,:IdCuartoUtil,:ValorCuartoUtil,:ValorMetrosCuadrados,:ValorParqueadero)');
			$Insert = $Db->prepare('INSERT INTO apartamentos VALUES(:NApartamento,NULL,:CuartoUtil,:IdEstado,:MetrosCuadrados,NULL,:Parqueadero,:ValorCuartoUtil,:ValorMetrosCuadrados,:ValorParqueadero)');
			$Insert->bindValue('NApartamento',$apartamento->getNApartamento());			
			$Insert->bindValue('CuartoUtil',$apartamento->getCuartoUtil());
			$Insert->bindValue('IdEstado',$apartamento->getIdEstado());
			$Insert->bindValue('MetrosCuadrados',$apartamento->getMetrosCuadrados());
			$Insert->bindValue('Parqueadero',$apartamento->getParqueadero());
			$Insert->bindValue('IdParqueadero',$apartamento->getIdParqueadero());
			$Insert->bindValue('IdCuartoUtil',$apartamento->getIdCuartoUtil());
			$Insert->bindValue('ValorCuartoUtil',$apartamento->getValorCuartoUtil());
			$Insert->bindValue('ValorMetrosCuadrados',$apartamento->getValorMetrosCuadrados());			
			$Insert->bindValue('ValorParqueadero',$apartamento->getValorParqueadero());			
			try{
				$Insert->execute();
				echo "<script>swal({
					title: 'Éxito',
					text: 'El apartamento se registro correctamente',
					type: 'success',
				  }, function(confirm){
					if(confirm){
					  window.location.href ='ListarApartamento.php?pagina=1';
					}
				  })
				</script>";
			}
			catch(Exception $e){
				echo $e->getMessage();
				echo "<script>swal({
					title: 'Error',
					text: 'Error al registrar el apartamento',
					type: 'error',
				  }, function(confirm){
					if(confirm){
					  window.location.href = 'IngresarApartamento.php?pagina=1';
					}
				  })
				</script>"; 
				die();
			}
		}

		//listar todos los registros de la tabla
        public function ListarApartamento(){
            $Db = Db::Conectar();
            $ListaApartamentos = [];
            $Sql = $Db->query('SELECT a.NApartamento,a.CuartoUtil,a.IdEstado,a.MetrosCuadrados,a.Parqueadero,a.ValorCuartoUtil,a.ValorMetrosCuadrados,a.ValorParqueadero,e.NombreEstado,p.TipoParqueadero from apartamentos a INNER JOIN estados e ON (a.IdEstado=e.IdEstado) INNER JOIN parqueaderos p ON (a.Parqueadero=p.IdParqueadero)');
			$Sql->execute();
			//Paginación

			$Total_filas = $Sql->rowCount();
            foreach($Sql->fetchAll() as $apartamento){
                $MyApartamento = new apartamento();

                $MyApartamento->setNApartamento($apartamento['NApartamento']);				
				$MyApartamento->setCuartoUtil($apartamento['CuartoUtil']);
				$MyApartamento->setIdEstado($apartamento['NombreEstado']);
				$MyApartamento->setMetrosCuadrados($apartamento['MetrosCuadrados']);
				$MyApartamento->setParqueadero($apartamento['TipoParqueadero']);
				$MyApartamento->setValorCuartoUtil($apartamento['ValorCuartoUtil']);
                $MyApartamento->setValorMetrosCuadrados($apartamento['ValorMetrosCuadrados']);
                $MyApartamento->setValorParqueadero($apartamento['ValorParqueadero']);                

                $ListaApartamentos[] = $MyApartamento;

            }
             return array($ListaApartamentos, $Total_filas);
		}

        public function ObtenerApartamento($NApartamento){ //Codigo para obtener un apartamento 
            $Db = Db::Conectar();
            $Sql = $Db->prepare('SELECT a.NApartamento,a.CuartoUtil,a.IdParqueadero, a.IdCuartoUtil,a.MetrosCuadrados,a.Parqueadero,a.ValorCuartoUtil,a.ValorMetrosCuadrados,a.ValorParqueadero,e.NombreEstado,a.IdEstado,p.TipoParqueadero from apartamentos a INNER JOIN estados e ON (a.IdEstado=e.IdEstado) INNER JOIN parqueaderos p ON (a.Parqueadero=p.IdParqueadero) WHERE NApartamento=:NApartamento');
            $Sql->bindValue('NApartamento', $NApartamento);
            $MyApartamento = new apartamento(); //crear un objeto de tipo apartamento
            try{
                $Sql->execute(); //Ejecutar el update
                $apartamento = $Sql->fetch(); //se almacena en la variable apartamento los datos de la variable sql
                $MyApartamento->setNApartamento($apartamento['NApartamento']);                
				$MyApartamento->setCuartoUtil($apartamento['CuartoUtil']);
				$MyApartamento->setIdEstado($apartamento['IdEstado']);
				$MyApartamento->setMetrosCuadrados($apartamento['MetrosCuadrados']);
				$MyApartamento->setParqueadero($apartamento['Parqueadero']);
				$MyApartamento->setIdParqueadero($apartamento['IdParqueadero']);
				$MyApartamento->setIdCuartoUtil($apartamento['IdCuartoUtil']);
				$MyApartamento->setValorCuartoUtil($apartamento['ValorCuartoUtil']);
                $MyApartamento->setValorMetrosCuadrados($apartamento['ValorMetrosCuadrados']);
                $MyApartamento->setValorParqueadero($apartamento['ValorParqueadero']);                
            }
            catch(Exception $e){ //Capturar errores
                echo $e->getMessage(); //Mostrar errores en la modificacion
                die();
            }

            return $MyApartamento;
        }

        public function ModificarApartamento($apartamento){
			$Db = Db::Conectar();
			//Definir la modificación a realizar.
			$Sql = $Db->prepare('UPDATE apartamentos SET NApartamento=:NApartamento ,CuartoUtil=:CuartoUtil, IdEstado=:IdEstado, MetrosCuadrados=:MetrosCuadrados, Parqueadero=:Parqueadero,IdParqueadero=:IdParqueadero,IdCuartoUtil=:IdCuartoUtil, ValorCuartoUtil=:ValorCuartoUtil ,ValorMetrosCuadrados=:ValorMetrosCuadrados, ValorParqueadero=:ValorParqueadero WHERE NApartamento=:NApartamento');
			$Sql->bindValue('NApartamento',$apartamento->getNApartamento());			
			$Sql->bindValue('CuartoUtil',$apartamento->getCuartoUtil());
			$Sql->bindValue('IdEstado',$apartamento->getIdEstado());
			$Sql->bindValue('MetrosCuadrados',$apartamento->getMetrosCuadrados());
			$Sql->bindValue('Parqueadero',$apartamento->getParqueadero());
			$Sql->bindValue('IdParqueadero',$apartamento->getIdParqueadero());
			$Sql->bindValue('IdCuartoUtil',$apartamento->getIdCuartoUtil());
			$Sql->bindValue('ValorCuartoUtil',$apartamento->getValorCuartoUtil());
			$Sql->bindValue('ValorMetrosCuadrados',$apartamento->getValorMetrosCuadrados());
			$Sql->bindValue('ValorParqueadero',$apartamento->getValorParqueadero());			
			try{
				$Sql->execute();
				echo "<script>swal({
					title: 'Éxito',
					text: 'El apartamento se actualizó correctamente',
					type: 'success',
				  }, function(confirm){
					if(confirm){
					  window.location.href ='../Vistas/ListarApartamento.php';
					}
				  })
				</script>";
			}
			catch(Exception $e){
				echo $e->getMessage();
				echo "<script>swal({
					title: 'Error',
					text: 'Debe asignar el número del Apto',
					type: 'error',
				  }, function(confirm){
					if(confirm){
					  window.location.href = '../Vistas/EditarApartamento.php';
					}
				  })
				</script>";
				die();
			}
		}

		public function CambiarDeEstado($NApartamento){
			$Db = Db::Conectar();
			$Sql = $Db->prepare('UPDATE apartamentos set IdEstado=2 WHERE NApartamento=:NApartamento');
			$Sql->bindValue('NApartamento',$NApartamento);
		   
			try{
				$Sql->execute();
	  
				echo "<script>swal({
				  title: 'Éxito',
				  text: 'El estado del apartamento se actualizó correctamente',
				  type: 'success',
				}, function(confirm){
				  if(confirm){
					window.location.href ='../Vistas/ListarApartamento.php';
				  }
				})
			  </script>";
	  
			}
			catch(Exeption $e){
				echo $e->getMessage();
				die();
			}
		}

		public function CambiarDeEstadoActivo($NApartamento){
			$Db = Db::Conectar();
			$Sql = $Db->prepare('UPDATE apartamentos set IdEstado=1 WHERE NApartamento=:NApartamento');
			$Sql->bindValue('NApartamento',$NApartamento);
		   
			try{
				$Sql->execute();
		
				echo "<script>swal({
				  title: 'Éxito',
				  text: 'El estado del apartamento se actualizó correctamente',
				  type: 'success',
				}, function(confirm){
				  if(confirm){
					window.location.href ='../Vistas/ListarApartamento.php';
				  }
				})
			  </script>";
		
			}
			catch(Exeption $e){
				echo $e->getMessage();
				die();
			}
		}
	}
?>
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
</html>