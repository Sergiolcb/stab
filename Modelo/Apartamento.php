<?php
	class apartamento{
		//Parámetros de entrada
		private $NApartamento;
		private $Parqueadero;
		private $CuartoUtil;
		private $IdEstado;
		private $MetrosCuadrados;
		private $ValorMetrosCuadrados;
		private $ValorParqueadero;
		private $ValorCuartoUtil;
		private $IdParqueadero;
		private $IdCuartoUtil;

		//Definir el constructor
		public function __construct(){}

		//Definir los métodos set y get para cada atributo de la clase
		public function setNApartamento($NApartamento){
			$this->NApartamento = $NApartamento;
		}

		public function getNApartamento(){
			return $this->NApartamento;
		}

		public function setParqueadero($Parqueadero){
			$this->Parqueadero = $Parqueadero;
		}

		public function getParqueadero(){
			return $this->Parqueadero;
		}

		public function setCuartoUtil($CuartoUtil){
			$this->CuartoUtil = $CuartoUtil;
		}

		public function getCuartoUtil(){
			return $this->CuartoUtil;
		}

		public function setIdEstado($IdEstado){
			$this->IdEstado = $IdEstado;
		}

		public function getIdEstado(){
			return $this->IdEstado;
		}

		public function setMetrosCuadrados($MetrosCuadrados){
			$this->MetrosCuadrados = $MetrosCuadrados;
		}

		public function getMetrosCuadrados(){
			return $this->MetrosCuadrados;
		}

		public function setValorMetrosCuadrados($ValorMetrosCuadrados){
			$this->ValorMetrosCuadrados = $ValorMetrosCuadrados;
		}

		public function getValorMetrosCuadrados(){
			return $this->ValorMetrosCuadrados;
		}
		public function setValorParqueadero($ValorParqueadero){
			$this->ValorParqueadero = $ValorParqueadero;
		}

		public function getValorParqueadero(){
			return $this->ValorParqueadero;
		}

		public function setValorCuartoUtil($ValorCuartoUtil){
			$this->ValorCuartoUtil = $ValorCuartoUtil;
		}

		public function getValorCuartoUtil(){
			return $this->ValorCuartoUtil;
		}

		public function setIdParqueadero($IdParqueadero){
			$this->IdParqueadero = $IdParqueadero;
		}

		public function getIdParqueadero(){
			return $this->IdParqueadero;
		}

		public function setIdCuartoUtil($IdCuartoUtil){
			$this->IdCuartoUtil = $IdCuartoUtil;
		}

		public function getIdCuartoUtil(){
			return $this->IdCuartoUtil;
		}
	}
?>