<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/styles.css" />
    <link rel="stylesheet" href="../css/bootstrap.min.css" />
    <title>Document</title>
</head>
<body>
    
<?php
require_once('../Modelo/Residente.php');
//require_once('../Modelo/DetalleResidente.php');
require_once('../Modelo/CrudResidente.php');
//require_once('../Modelo/CrudDetalleResidente.php');



$Residente = new Residente();
$CrudResidente = new CrudResidente();
//$DetalleResidente = new DetalleResidente();
//$CrudDetalleResidente = new CrudDetalleResidente();

if(isset($_POST["Registrar"])) // Si la peticion es registrar
{
    //Instanciar los atributos
    $Residente->setCedula($_POST["Cedula"]);
    $Residente->setNombre($_POST["Nombre"]);
    $Residente->setIdEstado($_POST["IdEstado"]);
    $Residente->setNApartamento($_POST["NApartamento"]);
    $Residente->setObservaciones($_POST["Observaciones"]);
    $Residente->setCorreo($_POST["Correo"]);
    $Residente->setTelefono($_POST["Telefono"]);
    $Residente->setCelular($_POST["Celular"]);

    $CodigoResidentegenerado = $CrudResidente::InsertarResidente($Residente); //Llamar el metodo para insertar
    if ($CodigoResidentegenerado > -1)
    {
           echo "<script>swal({
            title: 'Éxito',
            text: 'El residente se ha registrado exitosamente',
            type: 'success',
          }, function(confirm){
            if(confirm){
              window.location.href = '../Vistas/ListadoResidentes.php';
            }
          })
        </script>";
    
    
   
    
  }
}
else if(isset($_POST["Modificar"])) // Si la peticion es registrar
{
    
    //Instanciar los atributos
    $Residente->setCedula($_POST["Cedula"]);
    $Residente->setNombre($_POST["Nombre"]);
    $Residente->setNApartamento($_POST["NApartamento"]);
    $Residente->setObservaciones($_POST["Observaciones"]);
    $Residente->setCorreo($_POST["Correo"]);
    $Residente->setTelefono($_POST["Telefono"]);
    $Residente->setCelular($_POST["Celular"]);
      $CodigoResidenteeditado = $CrudResidente::EditarResidente($Residente); //Llamar el metodo para insertar
    if ($CodigoResidenteeditado > -1)
    { /*
        $edicionexitoso = 0;
        $DetalleResidente->setCedulaResidente($_POST["Cedula"]);
        $DetalleResidente->setCodigoEstado($_POST["Estado"]);
        $edicionexitoso=($CrudDetalleResidente::EditarDetalleResidente($DetalleResidente)); //Llamar el metodo para insertar
        if($edicionexitoso==0)
        { */
           echo "<script>swal({
            title: 'Éxito',
            text: 'El residente se ha modificado exitosamente',
            type: 'success',
          }, function(confirm){
            if(confirm){
              window.location.href = '../Vistas/ListadoResidentes.php';
            }
          })
        </script>";
       /* }  
        else
        {
            echo "<script>swal({
                title: 'Error',
                text: 'Problemas en el registro del Residente 2',
                type: 'error',
              })
            </script>"; 
        } */
    }
    
    else
    {
        echo "Problemas en la edición del Residente";
    }
}
else if($_GET['Accion']=="CambiarDeEstado")
{
  $CrudResidente::CambiarDeEstado($_GET["Cedula"]);
}

else if($_GET['Accion']=="CambiarDeEstadoActivo")
{
  $CrudResidente::CambiarDeEstadoActivo($_GET["Cedula"]);
}
?>

</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
</html>