<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/styles.css" />
    <link rel="stylesheet" href="../css/bootstrap.min.css" />
    <title>Document</title>
</head>
<body>
    
<?php
require_once('../Modelo/Propietario.php');
require_once('../Modelo/DetallePropietario.php');
require_once('../Modelo/CrudPropietario.php');
require_once('../Modelo/CrudDetallePropietario.php');



$Propietario = new Propietario();
$CrudPropietario = new CrudPropietario();
$DetallePropietario = new DetallePropietario();
$CrudDetallePropietario = new CrudDetallePropietario();

if(isset($_POST["Registrar"])) // Si la peticion es registrar
{
    //Instanciar los atributos
    $Propietario->setCedula($_POST["Cedula"]);
    $Propietario->setNombre($_POST["Nombre"]);
    $Propietario->setTelefono($_POST["Telefono"]);
    $Propietario->setDireccion($_POST["Direccion"]);
    $Propietario->setCorreo($_POST["Correo"]);
    $Propietario->setIdEstado($_POST["IdEstado"]);
    $Propietario->setCelular($_POST["Celular"]);
    if(isset($_POST["CACorreo"])){
      $Propietario->setRecibirCorreo($_POST["CACorreo"]);
    }
    if(isset($_POST["CAFisica"])){
      $Propietario->setRecibirFisico($_POST["CAFisica"]);
    }
  
    $CodigoPropietariogenerado = $CrudPropietario::InsertarPropietario($Propietario); //Llamar el metodo para insertar
      if ($CodigoPropietariogenerado > -1)
      {
          $regitroexitoso = 0;
          $apartamentos = $_POST['NApartamento'];
          foreach ($apartamentos as $NApartamento) {
            $DetallePropietario->setNApartamento($NApartamento);
            $DetallePropietario->setCedulaPropietario($_POST["Cedula"]);
            $regitroexitoso=($CrudDetallePropietario::InsertarDetallePropietario($DetallePropietario)); //Llamar el metodo para insertar 
          }
          if($regitroexitoso>0)
          {
             echo "<script>swal({
              title: 'Éxito',
              text: 'El propietario se ha registrado exitosamente',
              type: 'success',
            }, function(confirm){
              if(confirm){
                window.location.href = '../Vistas/ListadoPropietarios.php';
              }
            })
          </script>";
          } 
          else
          {
              echo "<script>swal({
                  title: 'Error',
                  text: 'Problemas en el registro del propietario',
                  type: 'error',
                }, function(confirm){
                  if(confirm){
                    window.location.href = '../Vistas/ListadoPropietarios.php';
                  }
                })
              </script>"; 
          }
      }       
}
else if(isset($_POST["Modificar"])) // Si la peticion es registrar
{
    
    //Instanciar los atributos
    $Propietario->setCedula($_POST["Cedula"]);
    $Propietario->setNombre($_POST["Nombre"]);
    $Propietario->setTelefono($_POST["Telefono"]);
    $Propietario->setDireccion($_POST["Direccion"]);
    $Propietario->setCorreo($_POST["Correo"]);
    $Propietario->setCelular($_POST["Celular"]);
    
    if ( isset($_POST["CACorreo"]) ){
      $Propietario->setRecibirCorreo($_POST["CACorreo"]);
      }
      if ( isset($_POST["CAFisica"]) ){
      $Propietario->setRecibirFisico($_POST["CAFisica"]);
      }    
      $CodigoPropietarioeditado = $CrudPropietario::EditarPropietario($Propietario); //Llamar el metodo para insertar
    if ($CodigoPropietarioeditado > -1)
    {
       
        $DetallePropietario->setCedulaPropietario($_POST["Cedula"]);
        $DetallePropietario->setNApartamento($_POST["NApartamento"]);
        $edicionexitoso=($CrudDetallePropietario::EditarDetallePropietario($DetallePropietario)); //Llamar el metodo para insertar
        if($edicionexitoso > -1)
        {
           echo "<script>swal({
            title: 'Éxito',
            text: 'El propietario se ha actualizado exitosamente',
            type: 'success',
          }, function(confirm){
            if(confirm){
              window.location.href = '../Vistas/ListadoPropietarios.php';
            }
          })
        </script>";
        } 
        else
        {
            echo "<script>swal({
                title: 'Error',
                text: 'Problemas en el editar propietario ',
                type: 'error',
              }, function(confirm){
                if(confirm){
                  window.location.href = '../Vistas/EditarPropietario.php';
                }
              })   
            </script>"; 
        }
    }
    
    else
    {
       
      echo "<script>swal({
        title: 'Error',
        text: 'Problemas en el editar propietario ',
        type: 'error',
      }, function(confirm){
        if(confirm){
          window.location.href = '../Vistas/EditarPropietario.php';
        }
      })   
    </script>"; 
    }
}
else if($_GET['Accion']=="CambiarDeEstado")
{
  $CrudDetallePropietario::CambiarDeEstado($_GET["Cedula"]);
}
 
else if($_GET['Accion']=="CambiarDeEstadoActivo")
{
  $CrudDetallePropietario::CambiarDeEstadoActivo($_GET["Cedula"]);
}
    
   
 

?>

</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
</html>