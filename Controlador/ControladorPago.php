<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/styles.css" />
    <link rel="stylesheet" href="../css/bootstrap.min.css" />
    <title>Document</title>
</head>
<body>
    
<?php
require_once('../Modelo/Pago.php');
require_once('../Modelo/CrudPago.php');
require_once('../Modelo/CrudAbonos.php');
require_once('../Modelo/Abonos.php');
$Pago = new Pago();
$CrudPago = new CrudPago();
$Abono = new Abono();
$CrudAbono = new CrudAbono();
if(isset($_POST["Registrar"])) // Si la peticion es registrar
{
    //Instanciar los atributos
    $Pago->setNCuentaCobro($_POST["NcuentaCobro"]);
    $Pago->setPeriodo($_POST["Periodo"]);
    $Pago->setPeriodoFin($_POST["PeriodoFin"]);
    $Pago->setFecha($_POST["FechaActual"]);
    $Pago->setFechaLimite($_POST["FechaLimite"]);
    $Pago->setNapartamento($_POST["NApartamento"]);
    $Pago->setPropietario($_POST["Propietario"]);
    $Pago->setDireccionEntrega($_POST["DireccionEntrega"]);
    $Pago->setCorreo($_POST["Email"]);
    $Pago->setValorParqueadero($_POST["Parqueadero"]);
    $Pago->setCuartoUtil($_POST["CuartoU"]);
    $Pago->setM2($_POST["M2"]);
    $Pago->setTotalPagar($_POST["TotalPagar"]);
    $Pago->setTipoPago($_POST["TipoPago"]);
    $Pago->setIdEstado($_POST["IdEstado"]);
    $Pago->setObservaciones($_POST["Observaciones"]);

   
    $CrudPago::InsertarPago($Pago); //Llamar el metodo para insertar
    
}

/*Pago completo */

else if(isset($_POST["AgregarPagoCompleto"])) // Si la peticion es agregar abono
{
    //Instanciar los atributos
    $Abono->setEstado($_POST["estado"]);
    $Abono->setIdPago($_POST["ncuentacobro"]);
    $Abono->setValorPago($_POST["pagocompleto"]);
   
    $CodigoAbonoAgregado = $CrudAbono::IngresarAbono($Abono); //Llamar el metodo para insertar
    

    if ($CodigoAbonoAgregado > -1)
    {
        $RegistroExitoso = 0;
        $Pago->setNCuentaCobro($_POST["ncuentacobro"]);
        $Pago->setIdEstado($_POST["estado"]);
        $RegistroExitoso=($CrudPago::ActualizarPago($Pago)); //Llamar el metodo para insertar
        if($RegistroExitoso==0)
        {
           echo "<script>swal({
            title: 'Éxito',
            text: 'El pago completo se ha registrado exitosamente',
            type: 'success',
          }, function(confirm){
            if(confirm){
              window.location.href = '../Vistas/ListadoPagos.php?pagina=1';
            }
          })
        </script>";
        } 
        else
        {
            echo "<script>swal({
                title: 'Error',
                text: 'Problemas en el registro del pago completo',
                type: 'error',
              }, function(confirm){
                if(confirm){
                  window.location.href = '../Vistas/ListadoPagos.php?pagina=1';
                }
              })
            </script>"; 
        }
    }
    
    else
    {
        echo "Problemas en el registro del abono";
    }

}

/*Agregar Abono */

else if(isset($_POST["AgregarAbono"])) // Si la peticion es agregar abono
{
    //Instanciar los atributos
    $Abono->setEstado($_POST["estado"]);
    $Abono->setIdPago($_POST["ncuentacobro"]);
    $Abono->setValorPago($_POST["abono"]);
   
    $CodigoAbonoAgregado = $CrudAbono::IngresarAbono($Abono); //Llamar el metodo para insertar
    

    if ($CodigoAbonoAgregado > -1)
    {
        $RegistroExitoso = 0;
        $Pago->setNCuentaCobro($_POST["ncuentacobro"]);
        $Pago->setIdEstado($_POST["estado"]);
        $RegistroExitoso=($CrudPago::ActualizarPago($Pago)); //Llamar el metodo para insertar
        if($RegistroExitoso==0)
        {
           echo "<script>swal({
            title: 'Éxito',
            text: 'El abono se ha registrado exitosamente',
            type: 'success',
          }, function(confirm){
            if(confirm){
              window.location.href = '../Vistas/ListadoPagos.php?pagina=1';
            }
          })
        </script>";
        } 
        else
        {
            echo "<script>swal({
                title: 'Error',
                text: 'Problemas en el registro del abono',
                type: 'error',
              }, function(confirm){
                if(confirm){
                  window.location.href = '../Vistas/ListadoPagos.php?pagina=1';
                }
              })
            </script>"; 
        }
    }
    
    else
    {
        echo "Problemas en el registro del propietario";
    }

}

?>

</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
</html>