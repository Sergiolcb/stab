<?php
//============================================================+
// File name   : example_014.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 014 for TCPDF class
//               Javascript Form and user rights (only works on Adobe Acrobat)
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Javascript Form and user rights (only works on Adobe Acrobat)
 * @author Nicola Asuni
 * @since 2008-03-04
 */


// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');
require_once('../../conexion.php');
require_once('../../Modelo/Pago.php');
require_once('../../Modelo/CrudPDF.php');


$CrudPDF = new CrudPDF();
$Pago = $CrudPDF::PDFPago($_GET["NCuentaCobro"]);

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);


// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));


// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);


// add a page
$pdf->AddPage();


// set default form properties

     
		
$pdf->setFormDefaultProp(array('lineWidth'=>1, 'borderStyle'=>'solid', 'fillColor'=>array(255, 255, 200), 'strokeColor'=>array(255, 128, 128)));

$pdf->SetFont('helvetica', 'BI', 18);
$pdf->Cell(0, 5, 'COMPROBANTE PAGO', 0, 1, 'C');
$pdf->Ln(10);

$pdf->SetFont('helvetica', '', 12);

$pdf->Cell(10, 5, '');
$pdf->SetFont('helvetica', 'B', 12);
$pdf->Cell(20, 5, 'Entidad:','C');
$pdf->SetFont('helvetica', '', 12);
$pdf->Cell(60, 5, 'Av. Villas');
$pdf->SetFont('helvetica', 'B', 12);
$pdf->Cell(35, 5, 'Tipo de cuenta:');
$pdf->SetFont('helvetica', '', 12);
$pdf->Cell(35, 5, 'Corriente');
$pdf->Ln(10);

$pdf->Cell(10, 5, '');
$pdf->SetFont('helvetica', 'B', 12);
$pdf->Cell(20, 5, 'Titular:');
$pdf->SetFont('helvetica', '', 12);
$pdf->Cell(60, 5, 'C.R. Altobelo P.H');
$pdf->SetFont('helvetica', 'B', 12);
$pdf->Cell(35, 5, 'N° Cuenta:');
$pdf->SetFont('helvetica', '', 12);
$pdf->Cell(35, 5, '50317248');
$pdf->Ln(20);


$pdf->Cell(10, 5, '');
$pdf->SetFont('helvetica', 'B', 12);
$pdf->Cell(35, 5, 'Cuenta de cobro:');
$pdf->SetFont('helvetica', '', 12);
$pdf->Cell(45, 5, $Pago->getNCuentaCobro());
$pdf->Ln(10);

$pdf->Cell(10, 5, '');
$pdf->SetFont('helvetica', 'B', 12);
$pdf->Cell(35, 5, 'Periodo Inicio:');
$pdf->SetFont('helvetica', '', 12);
$pdf->Cell(45, 5, $Pago->getPeriodo());
$pdf->SetFont('helvetica', 'B', 12);
$pdf->Cell(35, 5, 'Periodo Fin:');
$pdf->SetFont('helvetica', '', 12);
$pdf->Cell(35, 5, $Pago->getPeriodoFin());
$pdf->Ln(10);

$pdf->Cell(10, 5, '');
$pdf->SetFont('helvetica', 'B', 12);
$pdf->Cell(35, 5, 'Fecha:');
$pdf->SetFont('helvetica', '', 12);
$pdf->Cell(45, 5, $Pago->getFecha());
$pdf->SetFont('helvetica', 'B', 12);
$pdf->Cell(35, 5, 'Fecha límite:');
$pdf->SetFont('helvetica', '', 12);
$pdf->Cell(35, 5, $Pago->getFechaLimite());
$pdf->Ln(10);

$pdf->Cell(10, 5, '');
$pdf->SetFont('helvetica', 'B', 12);
$pdf->Cell(35, 5, 'N° apartamento:');
$pdf->SetFont('helvetica', '', 12);
$pdf->Cell(45, 5, $Pago->getNApartamento());
$pdf->SetFont('helvetica', 'B', 12);
$pdf->Cell(35, 5, 'Propietario:');
$pdf->SetFont('helvetica', '', 12);
$pdf->Cell(35, 5, $Pago->getPropietario());
$pdf->Ln(10);

$pdf->Cell(10, 5, '');
$pdf->SetFont('helvetica', 'B', 12);
$pdf->Cell(35, 5, 'Dirección:');
$pdf->SetFont('helvetica', '', 12);
$pdf->Cell(45, 5, $Pago->getDireccionEntrega());
$pdf->SetFont('helvetica', 'B', 12);
$pdf->Cell(35, 5, 'Email:');
$pdf->SetFont('helvetica', '', 12);
$pdf->Cell(35, 5, $Pago->getCorreo());
$pdf->Ln(20);


$pdf->SetFont('helvetica', 'B', 12);
$pdf->Cell(0, 5, 'Conceptos que intervienen en la cuota de administración', 0, 1, 'C');
$pdf->Ln(10);

$pdf->Cell(10, 5, '');
$pdf->SetFont('helvetica', 'B', 12);
$pdf->Cell(65, 5, 'Concepto');
$pdf->SetFont('helvetica', 'B', 12);
$pdf->Cell(55, 5, 'Valor');
$pdf->SetFont('helvetica', 'B', 12);
$pdf->Cell(35, 5, 'Observaciones');
$pdf->Ln(15);

$pdf->Cell(10, 5, '');
$pdf->SetFont('helvetica', '', 12);
$pdf->Cell(65, 5, 'Parqueadero');
$pdf->Cell(40, 5, $Pago->getValorParqueadero());
$pdf->SetFont('helvetica', '', 12);
$pdf->Cell(35, 5, $Pago->getObservaciones());


$pdf->Ln(15);
$pdf->Cell(10, 5, '');
$pdf->SetFont('helvetica', '', 12);
$pdf->Cell(65, 5, 'Cuarto útil');
$pdf->Cell(40, 5, $Pago->getCuartoUtil());
$pdf->Ln(15);
$pdf->Cell(10, 5, '');
$pdf->SetFont('helvetica', '', 12);
$pdf->Cell(65, 5, 'M2');
$pdf->Cell(40, 5, $Pago->getM2());
$pdf->Ln(15);
$pdf->Cell(10, 5, '');
$pdf->SetFont('helvetica', 'B', 12);
$pdf->Cell(65, 5, 'Total a pagar:');
$pdf->SetFont('helvetica', '', 12);
$pdf->Cell(40, 5, $Pago->getTotalPagar());
$pdf->Ln(15);

// ---------------------------------------------------------

//Close and output PDF document
ob_end_clean();
$pdf->Output('Informe'.$Pago->getNCuentaCobro().'.pdf', 'D');
//============================================================+
// END OF FILE
//============================================================+
