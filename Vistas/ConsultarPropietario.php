<?php
session_start();
if(!(isset($_SESSION["usuario"]))){
  header("Location: ../index.php");
}
require_once('../Modelo/CrudPropietario.php');
require_once('../Modelo/Propietario.php');
require_once('../Modelo/DetallePropietario.php');
require_once('../Modelo/CrudDetallePropietario.php');

$CrudPropietario = new CrudPropietario();
$Propietario = $CrudPropietario::MostrarPropietario($_GET["Cedula"]);
$CrudDetallePropietario = new CrudDetallePropietario();
$DetallePropietario = $CrudDetallePropietario::MostrarDetalle($_GET["Cedula"]);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/styles.css" />
    <link rel="stylesheet" href="../css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <script src="http://code.jquery.com/jquery-latest.js"></script>
  <script src="../../js/menu.js"></script>
    <title>Consultar propietario</title>
</head>
<body>
<header>
<div class="menu_bar">
      <a href="#" class="btn-menu"><span class="icon-menu"></span>Menu</a>
    </div>
    <div class="area"></div><nav class="main-menu">
            <ul>
            <li>
              
                      <img src="../img/logo2.png" alt="1" id="iconos-menu2">              
                </li>
                <br>
                <li>
                    <a href="Inicio.php">
                        <i class="fa fa-home fa-2x" id="iconos-menu"></i>
                        <span class="nav-text" >
                            Inicio
                        </span>
                    </a>

                  <li class="has-subnav">
                    <a href="ListarApartamento.php?pagina=1">
                    <i class="fas fa-building fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Apartamentos                            
                        </span>
                    </a>                    
                </li>

                </li>
                <li class="has-subnav">
                    <a href="ListadoPropietarios.php?pagina=1">
                    <i class="fas fa-user-tie fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Propietarios
                            
                        </span>
                    </a>
                    
                </li>
                <li class="has-subnav">
                    <a href="ListadoResidentes.php?pagina=1">
                       <i class="fas fa-user-alt fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Residentes
                        </span>
                    </a>
                    
                </li>
                <li class="has-subnav">
                    <a href="ListadoPagos.php?pagina=1">
                       <i class="fas fa-file-invoice-dollar fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Pagos
                        </span>
                    </a>
                   
                </li>
                <li>
                    <a href="Informes.php?pagina=1">
                        <i class="fa fa-bar-chart-o fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Informes
                        </span>
                    </a>
                </li>
                
            </ul>

            <ul class="logout">
                <li>
                   <a href="../CerrarSesion.php">
                         <i class="fa fa-power-off fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Cerrar sesión
                        </span>
                    </a>
                </li>  
            </ul>
        </nav>
</div>
    </header>
<h1 align="center">CONSULTAR PROPIETARIO</h1> <br><br>
<form  class="form-horizontal" action="../Controlador/ControladorPropietario.php" method="post" style="align-content: center" id="FrmPropietarios" name="FrmPropietarios">
<div class="form-group">
        <label for="radio" class="control-label col-xs-3"><label style="color: red;" for="" id="ValidarNombre"></label>Nombre:</label> 
        <div class="col-xs-2">
        <input id="Nombre" name="Nombre" readonly value="<?php echo $Propietario->getNombre();?>" class="form-control" type="text"> 
        </div>
        <label for="radio" class="control-label col-xs-3"><label style="color: red;" for="" id="ValidarCedula"></label>Cédula:</label> 
        <div class="col-xs-2">
        <input id="Cedula" readonly value="<?php echo $Propietario->getCedula();?>" name="Cedula" class="form-control" type="text">
        </div>
    </div>

    <div class="form-group">
        <label for="radio" class="control-label col-xs-3"><label style="color: red;" for=""></label>Teléfono:</label> 
        <div class="col-xs-2">
        <input id="Telefono" readonly value="<?php echo $Propietario->getTelefono();?>" name="Telefono" class="form-control" type="text">
        </div>
        <label for="radio" class="control-label col-xs-3"><label style="color: red;" for="" ></label>Celular:</label> 
        <div class="col-xs-2">
        <input id="Celular" readonly value="<?php echo $Propietario->getCelular();?>" name="Celular" class="form-control" type="text">
        </div>
       
       
    </div>

    <div class="form-group">
        <label for="radio" class="control-label col-xs-3">Correo:</label> 
        <div class="col-xs-2">
        <input id="Correo" readonly value="<?php echo $Propietario->getCorreo();?>" name="Correo" class="form-control" type="text">
        </div>
        <label for="radio" class="control-label col-xs-3">Dirección:</label> 
        <div class="col-xs-2">
        <input id="Direccion" readonly value="<?php echo $Propietario->getDireccion();?>" name="Direccion" class="form-control" type="text">
        </div>
        </div>
        <div class="form-group">
        <label for="radio" class="control-label col-xs-3"><label style="color: red;" for="" id="ValidarNApartamentoP"></label> N° Apartamento:</label> 
        <div class="col-xs-2">
        <input id="NApartamento" readonly value="<?php echo $DetallePropietario->getNApartamento();?>" name="NApartamento" class="form-control" type="text">
        </div>
   
        <label for="radio" class="control-label col-xs-3">Estado:</label> 
        <div class="col-xs-2">
        <input id="IdEstado" readonly value="<?php echo $Propietario->getIdEstado();?>" name="IdEstado" class="form-control" type="text">
        </div>
    </div>

    <div class="form-group">
        <label for="radio" class="control-label col-xs-6">¿Desea recibir la cuota de administración por correo?</label> 
        <div class="col-xs-2">
        <p name="CACorreo" id="CACorreo" style="position: relative;top:7px;">
        <?php 
        if  ($Propietario->getRecibirCorreo()=="on")
        {
            echo "Si";
        }
        else
        {
            echo "No";
        }
        ?> </p>
        </div>
    </div>

    <div class="form-group">
        <label for="radio" class="control-label col-xs-6">¿Desea recibir la cuota de administración física?</label> 
        <div class="col-xs-2">
        <p name="CAFisica" id="CAFisica" style="position: relative;top:7px;">
        <?php 
        if  ($Propietario->getRecibirFisico()=="on")
        {
            echo "Si";
        }
        else
        {
            echo "No";
        }
        ?> </p>
        </div>
    </div>
    <center>
    <br>
    <br>
    <br>
    <button type="button" onclick="VolverP()" class="btn btn-info">Volver</button>
    </center>
    </form>

    <p align="center" id="RespuestaTransaccionPropietario"></p>
    <br><br><br>
    <footer align="center" class="border-top footer" style="font-family:fantasy">
        <div class="container">
            &copy; 2020 - S.T.A.B
        </div>
    </footer>
        </center>
        
    
</body>
<script src="../js/funciones.js"></script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/acf5d1b9db.js" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
</html>