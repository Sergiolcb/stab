<?php
require_once('../conexion.php'); 
require_once('../Modelo/CrudApartamento.php'); //Incluir el modelo crud apartamento
require_once('../Modelo/Apartamento.php');

$CrudApartamento = new CrudApartamento();
list($ListasApartamentos, $Total_filas) = $CrudApartamento->ListarApartamento(); //llamado al metodo ListarApartamento
//var_dump($ListasApartamentos);
$filas_por_pagina = 10;

//contar el número de filas

$paginas = $Total_filas/$filas_por_pagina;
$paginas = ceil($paginas);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Apartamentos</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/styles.css" />
    <link rel="stylesheet" href="../css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script> 
  <script>
    $(document).ready(function () {
   $('#entradafilter').keyup(function () {
      var rex = new RegExp($(this).val(), 'i');
        $('.contenidobusqueda tr').hide();
        $('.contenidobusqueda tr').filter(function () {
            return rex.test($(this).text());
        }).show();

        })

});
</script>
</head>
<body>
    <div class="area"></div><nav class="main-menu">
            <ul>
            <li>
              
                      <img src="../img/logo2.png" alt="1" id="iconos-menu2">              
                </li>
                <br>
                <li>
                    <a href="Inicio.php">
                        <i class="fa fa-home fa-2x" id="iconos-menu"></i>
                        <span class="nav-text" >
                            Inicio
                        </span>
                    </a>

                  <li class="has-subnav">
                    <a href="ListarApartamento.php?pagina=1">
                    <i class="fas fa-building fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Apartamentos                            
                        </span>
                    </a>                    
                </li>

                </li>
                <li class="has-subnav">
                    <a href="ListadoPropietarios.php?pagina=1">
                    <i class="fas fa-user-tie fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Propietarios
                            
                        </span>
                    </a>
                    
                </li>
                <li class="has-subnav">
                    <a href="ListadoResidentes.php?pagina=1">
                       <i class="fas fa-user-alt fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Residentes
                        </span>
                    </a>
                    
                </li>
                <li class="has-subnav">
                    <a href="ListadoPagos.php?pagina=1">
                       <i class="fas fa-file-invoice-dollar fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Pagos
                        </span>
                    </a>
                   
                </li>
                <li>
                    <a href="Informes.php?pagina=1">
                        <i class="fa fa-bar-chart-o fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Informes
                        </span>
                    </a>
                </li>
                
            </ul>

            <ul class="logout">
                <li>
                   <a href="../CerrarSesion.php">
                         <i class="fa fa-power-off fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Cerrar sesión
                        </span>
                    </a>
                </li>  
            </ul>
        </nav>
</div>
  <center>
   <h1 style="font-family:fantasy">APARTAMENTOS</h1>
    <div class="content">
        <div class="leftboton">
            <a href="IngresarApartamento.php" title="Agregar Apartamento"><i class="far fa-plus-square fa-3x"></i></a>
        </div>
        <div class="buscar">
            <label for="radio" class="control-label col-xs-3">Buscar:</label>
            <div class="col-xs-6">
                <input id="entradafilter" type="text" class="form-control">
            </div>
        </div>
    </div>
    <br>
   <table class="table" style="width:80%">
   <thead class="thead-dark">
   <tr>
   	<th scope="col">Número apartamento</th>
   	<th scope="col">Parqueadero</th>
    <th scope="col">Valor Cuota de administración</th>
    <th scope="col">Estado</th>
   	<th scope="col">Acciones</th>
   </tr>
   </thead>

   <tbody class="contenidobusqueda">
   	<?php
        //PAGINACIÓN
        $Db = Db::Conectar();
        $ListaPagoFilas = [];
        $iniciar = ($_GET['pagina']-1)*$filas_por_pagina;
        $Sql_filas ='SELECT a.NApartamento,a.CuartoUtil,a.IdEstado,a.MetrosCuadrados,a.Parqueadero,a.ValorCuartoUtil,a.ValorMetrosCuadrados,a.ValorParqueadero,e.NombreEstado,p.TipoParqueadero from apartamentos a 
        INNER JOIN estados e ON (a.IdEstado=e.IdEstado) 
        INNER JOIN parqueaderos p ON (a.Parqueadero=p.IdParqueadero) LIMIT :iniciar,:nfilas';
        $sentencia_filas = $Db->query('SET lc_time_names = "es_ES";');
        $sentencia_filas = $Db->prepare($Sql_filas);
        $sentencia_filas->bindParam(':iniciar', $iniciar, PDO::PARAM_INT);
        $sentencia_filas->bindParam(':nfilas', $filas_por_pagina, PDO::PARAM_INT);
        $sentencia_filas->execute();
        
        $resultado_articulos = $sentencia_filas->fetchAll();
        //FIN PAGINACIÓN
   	foreach($resultado_articulos as $apartamento){
        $Valor = $apartamento["ValorMetrosCuadrados"];
   		?>
        <tr>

   		<td align="center"><?php echo $apartamento["NApartamento"]; ?></td>
   		<td><?php echo $apartamento["TipoParqueadero"]; ?></td>
      <td><?php echo '$' . number_format($Valor, 2); ?></td>
      <td><?php echo $apartamento["NombreEstado"]; ?></td>
   	  <td>
        <a href="ConsultarApartamento.php?NApartamento=<?php echo $apartamento["NApartamento"]; ?>" title="Consultar Apartamento"><i class="fas fa-eye fa-lg"></i></a>
        <a href="EditarApartamento.php?NApartamento=<?php echo $apartamento["NApartamento"]; ?>" title="Editar Apartamento"><i class="fas fa-pen fa-lg"></i></a>
        <?php
            if ($apartamento["NombreEstado"]=='Activo')
            {?>
            <a href="../Controlador/ControladorApartamento.php?NApartamento=<?php echo $apartamento["NApartamento"];?>&Accion=CambiarDeEstado" title="Cambiar de estado"><i class="fas fa-exchange-alt fa-lg"></i></a>  
            </td>  
            </tr>
        <?php
        }
        else if ($apartamento["NombreEstado"]=='Inactivo')
        {?>
            <a href="../Controlador/ControladorApartamento.php?NApartamento=<?php echo $apartamento["NApartamento"];?>&Accion=CambiarDeEstadoActivo" title="Cambiar de estado"><i class="fas fa-exchange-alt fa-lg"></i></a>  
            </td>  
            </tr>
        <?php
        }
    }
    ?>
   	

   </tbody>
</table>
<footer class="border-top footer" style="font-family:fantasy">
        <div class="container">
            &copy; 2020 - S.T.A.B
        </div>
    </footer>
<nav aria-label="Page navigation example">
  <ul class="pagination justify-content-center">

    <li class="page-item <?php echo($_GET['pagina']<=1 ? 'disabled': '') ?>">
    <a class="page-link" href="ListarApartamento.php?pagina=<?php echo($_GET['pagina']-1); ?>">Anterior</a></li>

    <?php
        for ($i=0; $i < $paginas; $i++):
    ?>
    <li class="page-item <?php echo($_GET['pagina']==$i+1 ? 'active' : '') ?>">
    <a class="page-link" href="ListarApartamento.php?pagina=<?php echo($i+1); ?>"><?php echo($i+1); ?></a></li>
    <?php endfor ?>
    <li class="page-item <?php echo($_GET['pagina']>=$paginas ? 'disabled': '') ?>">
    <a class="page-link" href="ListarApartamento.php?pagina=<?php echo($_GET['pagina']+1); ?>">Siguiente</a></li>
  </ul>
</nav>
</center>

</body>
<script src="../js/funciones.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/acf5d1b9db.js" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
</html>