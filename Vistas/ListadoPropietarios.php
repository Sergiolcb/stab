<?php
session_start();
if(!(isset($_SESSION["usuario"]))){
  header("Location: ../index.php");
}
require_once('../conexion.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/styles.css" />
    <link rel="stylesheet" href="../css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script> 

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/js/bootstrap-select.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

    <title>Lista</title>
</head>
<body>
<div class="area"></div><nav class="main-menu">
            <ul>
            <li>
              
                      <img src="../img/logo2.png" alt="1" id="iconos-menu2">              
                </li>
                <br>
                <li>
                    <a href="Inicio.php">
                        <i class="fa fa-home fa-2x" id="iconos-menu"></i>
                        <span class="nav-text" >
                            Inicio
                        </span>
                    </a>

                  <li class="has-subnav">
                    <a href="ListarApartamento.php?pagina=1">
                    <i class="fas fa-building fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Apartamentos                            
                        </span>
                    </a>                    
                </li>

                </li>
                <li class="has-subnav">
                    <a href="ListadoPropietarios.php?pagina=1">
                    <i class="fas fa-user-tie fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Propietarios
                            
                        </span>
                    </a>
                    
                </li>
                <li class="has-subnav">
                    <a href="ListadoResidentes.php?pagina=1">
                       <i class="fas fa-user-alt fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Residentes
                        </span>
                    </a>
                    
                </li>
                <li class="has-subnav">
                    <a href="ListadoPagos.php?pagina=1">
                       <i class="fas fa-file-invoice-dollar fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Pagos
                        </span>
                    </a>
                   
                </li>
                <li>
                    <a href="Informes.php?pagina=1">
                        <i class="fa fa-bar-chart-o fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Informes
                        </span>
                    </a>
                </li>
                
            </ul>

            <ul class="logout">
                <li>
                   <a href="../CerrarSesion.php">
                         <i class="fa fa-power-off fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Cerrar sesión
                        </span>
                    </a>
                </li>  
            </ul>
        </nav>
</div>

  <center>
    <h1 style="font-family:fantasy">PROPIETARIOS</h1>
    <div class="content">
                    <div class="leftboton">
                    <a href="Propietarios.php" title="Agregar propietario"><i class="far fa-plus-square fa-3x"></i></a>
    </div>
    <div class="buscar">
<label for="radio" class="control-label col-xs-3">Buscar:</label>
    <div class="col-xs-6">
        <input id="entradafilter" type="text" class="form-control">
    </div>
</div></div>
    
<table class="table" style="width:80%">
  <thead>
    <tr>
      <th scope="col">Cédula</th>
      <th scope="col">Nombre</th>
      <th scope="col">Teléfono</th>
      <th scope="col">Correo</th>
      <th scope="col">Estado</th>
      <th scope="col">Apartamento</th>
      <th scope="col">Opciones</th>
    </tr>
  </thead>
  <tbody class="contenidobusqueda">
  <?php
        $Db = Db::Conectar();
        $filas_por_pagina = 10;
        
        $iniciar = ($_GET['pagina']-1)*$filas_por_pagina;
       
        //paginación
        $Sql_filas = 'SELECT p.Cedula,p.Nombre,p.Telefono,p.Direccion,p.Correo,e.NombreEstado,da.NApartamento from propietarios p 
        INNER JOIN estados e ON (p.IdEstado=e.IdEstado)
        INNER JOIN detallepropietarioapartamento da ON
        da.CedulaPropietario = p.Cedula LIMIT :iniciar,:nfilas';
        $sentencia_filas = $Db->prepare($Sql_filas);
        $sentencia_filas->bindParam(':iniciar', $iniciar, PDO::PARAM_INT);
        $sentencia_filas->bindParam(':nfilas', $filas_por_pagina, PDO::PARAM_INT);
        $sentencia_filas->execute();

        //$resultado_filas = $sentencia_filas->fetchAll();
        //fin paginación
        $Sql = $Db->query('SELECT p.Cedula,p.Nombre,p.Telefono,p.Direccion,p.Correo,e.NombreEstado,da.NApartamento from propietarios p 
        INNER JOIN estados e ON (p.IdEstado=e.IdEstado)
        INNER JOIN detallepropietarioapartamento da ON
        da.CedulaPropietario = p.Cedula');
        $Sql->execute();
        //contar el número de filas
        $Total_filas = $Sql->rowCount();

        $paginas = $Total_filas/$filas_por_pagina;
        $paginas = ceil($paginas);
        while($row=$sentencia_filas->fetch(PDO::FETCH_ASSOC))
        {
        extract((array)$row);
        ?>
        <tr>
            <td><?php echo $row["Cedula"];?></td>
            <td><?php echo $row["Nombre"];?></td>
            <td><?php echo $row["Telefono"];?></td>
            <td><?php echo $row["Correo"];?></td>
            <td><?php echo $row["NombreEstado"];?></td>
            <td align="center"><?php echo $row["NApartamento"];?></td>
            <td><a href="ConsultarPropietario.php?Cedula=<?php echo $row["Cedula"]; ?>" title="Consultar propietario"><i class="fas fa-eye fa-lg"></i></a>
            <a href="EditarPropietario.php?Cedula=<?php echo $row["Cedula"]; ?>" title="Editar propietario"><i class="fas fa-pen fa-lg"></i></a>
            <?php
            if ($row["NombreEstado"]=='Activo')
            {?>
            <a href="../Controlador/ControladorPropietario.php?Cedula=<?php echo $row["Cedula"];?>&Accion=CambiarDeEstado" title="Cambiar de estado"><i class="fas fa-exchange-alt fa-lg"></i></a>  
            </td>  
            </tr>
        <?php
        }
        else if ($row["NombreEstado"]=='Inactivo')
        {?>
            <a href="../Controlador/ControladorPropietario.php?Cedula=<?php echo $row["Cedula"];?>&Accion=CambiarDeEstadoActivo" title="Cambiar de estado"><i class="fas fa-exchange-alt fa-lg"></i></a>  
            </td>  
            </tr>
        <?php
        }
    }
    ?>
    
  </tbody>
</table>
<nav aria-label="Page navigation example">
  <ul class="pagination justify-content-center">

    <li class="page-item <?php echo($_GET['pagina']<=1 ? 'disabled': '') ?>">
    <a class="page-link" href="ListadoPropietarios.php?pagina=<?php echo($_GET['pagina']-1); ?>">Anterior</a></li>

    <?php
        for ($i=0; $i < $paginas; $i++):
    ?>
    <li class="page-item <?php echo($_GET['pagina']==$i+1 ? 'active' : '') ?>">
    <a class="page-link" href="ListadoPropietarios.php?pagina=<?php echo($i+1); ?>"><?php echo($i+1); ?></a></li>
    <?php endfor ?>
    <li class="page-item <?php echo($_GET['pagina']>=$paginas ? 'disabled': '') ?>">
    <a class="page-link" href="ListadoPropietarios.php?pagina=<?php echo($_GET['pagina']+1); ?>">Siguiente</a></li>
  </ul>
</nav>
</center>
<footer align="center" class="border-top footer" style="font-family:fantasy">
        <div class="container">
            &copy; 2020 - S.T.A.B
        </div>
    </footer>
</body>
<script src="../js/funciones.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/acf5d1b9db.js" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
</html>