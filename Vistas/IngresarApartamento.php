<?php
	require_once('../conexion.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registrar Apartamentos</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/styles.css" />
    <link rel="stylesheet" href="../css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/js/bootstrap-select.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
</head>
<body>
  <header>
    <div class="menu_bar">
      <a href="#" class="btn-menu"><span class="icon-menu"></span>Menu</a>
    </div>
    <div class="area"></div><nav class="main-menu">
            <ul>
            <li>
              
                      <img src="../img/logo2.png" alt="1" id="iconos-menu2">              
                </li>
                <br>
                <li>
                    <a href="Inicio.php">
                        <i class="fa fa-home fa-2x" id="iconos-menu"></i>
                        <span class="nav-text" >
                            Inicio
                        </span>
                    </a>

                  <li class="has-subnav">
                    <a href="ListarApartamento.php?pagina=1">
                    <i class="fas fa-building fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Apartamentos                            
                        </span>
                    </a>                    
                </li>

                </li>
                <li class="has-subnav">
                    <a href="ListadoPropietarios.php?pagina=1">
                    <i class="fas fa-user-tie fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Propietarios
                            
                        </span>
                    </a>
                    
                </li>
                <li class="has-subnav">
                    <a href="ListadoResidentes.php?pagina=1">
                       <i class="fas fa-user-alt fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Residentes
                        </span>
                    </a>
                    
                </li>
                <li class="has-subnav">
                    <a href="ListadoPagos.php?pagina=1">
                       <i class="fas fa-file-invoice-dollar fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Pagos
                        </span>
                    </a>
                   
                </li>
                <li>
                    <a href="Informes.php?pagina=1">
                        <i class="fa fa-bar-chart-o fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Informes
                        </span>
                    </a>
                </li>
                
            </ul>

            <ul class="logout">
                <li>
                   <a href="../CerrarSesion.php">
                         <i class="fa fa-power-off fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Cerrar sesión
                        </span>
                    </a>
                </li>  
            </ul>
        </nav>
  </header>
<div id="body">
<h1 align="center">INGRESAR APARTAMENTO</h1>
<br><br>
<center>
<form class="form-horizontal" action="../Controlador/ControladorApartamento.php" style="align-content: center" method="POST" id="FrmApartamentos" name="FrmApartamentos">
<div class="form-group">
  <label for="radio" class="control-label col-xs-4"><label style="color: red;" for="" id="ValidarNApartamento"></label>Número de apartamento:</label> 
    <div class="col-xs-2">
      <input class="form-control" type="text" name="NApartamento" id="NApartamento">
    </div>
</div>
<div class="form-group">
  <label for="radio" class="control-label col-xs-4"><label style="color: red;" for="" id="ValidarMetrosCuadrados"></label>Coeficiente de copropiedad:</label> 
    <div class="col-xs-2">
      <input class="form-control" type="text" name="MetrosCuadrados" id="MetrosCuadrados" value="0">
    </div>
  <label for="radio" class="control-label col-xs-2"><label style="color: red;" for="" id="ValidarValorMetrosCuadrados"></label>Valor cuota de administración:</label> 
    <div class="col-xs-2">
      <input class="form-control" type="text" name="ValorMetrosCuadrados" id="ValorMetrosCuadrados" value="0">
    </div>
</div>

<div class="form-group">
<label for="radio" class="control-label col-xs-4">Parqueadero:</label> 
    <div class="col-xs-2">
      <select name="Parqueadero" id="Parqueadero" class="form-control" onclick="validar();">
          <option value="0">--Ninguno--</option>
            <?php 
             $Db = Db::Conectar();
             $Sql = $Db->query('SELECT * FROM parqueaderos');
             $Sql->execute();
             while($row=$Sql->fetch(PDO::FETCH_ASSOC))
             {
             extract((array)$row);
            ?>
          <option value="<?php echo $row['IdParqueadero']; ?>"><?php echo $row['TipoParqueadero']; ?></option>
            <?php
             }
            ?>
      </select>
    </div>
</div>
<div class="form-group">
<label for="radio" class="control-label col-xs-4"><label style="color: red;" for="" id="ValidarNombre"></label>Valor parqueadero:</label> 
    <div class="col-xs-2">
      <input class="form-control" type="text" disabled name="ValorParqueadero" id="ValorParqueadero" value="0">
    </div>
    <label for="radio" class="control-label col-xs-2"><label style="color: red;" for="" id="ValidarIdentificadorP"></label>Identificador de parqueadero:</label> 
    <div class="col-xs-2">
      <input class="form-control" type="text" maxlength="10" name="IdParqueadero" id="IdParqueadero">
    </div>
</div>
<div class="form-group">
  
  <label for="radio" class="control-label col-xs-4"><label style="color: red;" for="" id="ValidarIdentificadorC"></label>Identificador de cuarto útil:</label> 
    <div class="col-xs-2">
      <input class="form-control" type="text" maxlength="10" name="IdCuartoUtil" id="IdCuartoUtil">
    </div>
</div>

<div class="form-group">
    <input type="hidden" name="IdEstado" id="IdEstado" value="1">
  <label for="radio" class="control-label col-xs-4"><label style="color: red;" for="" id="ValidarNombre"></label>Cuarto útil:</label> 
    <div class="col-xs-2">
      <select name="CuartoUtil" id="CuartoUtil" class="form-control" onclick="validar();">
          <option value="No">--Ninguno--</option>
          <option value="Sí">Sí</option>
          <option value="No">No</option>
      </select>
    </div>
    <label for="radio" class="control-label col-xs-2" id="LabelValorCuartoUtil"><label style="color: red;" for="" id="ValidarNombre"></label>Valor cuarto útil:</label> 
    <div class="col-xs-2">
      <input class="form-control" type="text" disabled name="ValorCuartoUtil" id="ValorCuartoUtil" value="0">
    </div>
</div>


  <center>
  <br><br><br>
	  <input type="hidden" name="Registrar">
	  <button class="btn btn-success" type="submit">Registrar</button>
    <button type="button" class="btn btn-info" onclick="VolverAp()">Volver</button>
  </center>
</form><br>
    <br><br><br><p align="center" id="RespuestaApartamento"></p>
</form>
<p align="center" id="RespuestaTransaccionApartamento"></p><br>
    <br><br><br>
    <footer class="border-top footer" style="font-family:fantasy">
        <div class="container">
            &copy; 2020 - S.T.A.B
        </div>
    </footer>
</center>
</div>
</body>
<script src="../js/funciones.js"></script>
<script src="https://kit.fontawesome.com/acf5d1b9db.js" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
</html>