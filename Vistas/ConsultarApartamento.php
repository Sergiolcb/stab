<?php
require_once('../Modelo/Apartamento.php');
require_once('../Modelo/CrudApartamento.php');

$MyApartamento = new CrudApartamento(); // crear un objeto de tipo CrudApartamento 
$apartamento = $MyApartamento::ObtenerApartamento($_GET["NApartamento"]);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ingresar Apartamento</title>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/styles.css" />
    <link rel="stylesheet" href="../css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <script src="http://code.jquery.com/jquery-latest.js"></script>
  <script src="../../js/menu.js"></script>
</head>
<body>
  <header>
    <div class="menu_bar">
      <a href="#" class="btn-menu"><span class="icon-menu"></span>Menu</a>
    </div>
    <div class="area"></div><nav class="main-menu">
            <ul>
            <li>
              
                      <img src="../img/logo2.png" alt="1" id="iconos-menu2">              
                </li>
                <br>
                <li>
                    <a href="Inicio.php">
                        <i class="fa fa-home fa-2x" id="iconos-menu"></i>
                        <span class="nav-text" >
                            Inicio
                        </span>
                    </a>

                  <li class="has-subnav">
                    <a href="ListarApartamento.php?pagina=1">
                    <i class="fas fa-building fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Apartamentos                            
                        </span>
                    </a>                    
                </li>

                </li>
                <li class="has-subnav">
                    <a href="ListadoPropietarios.php?pagina=1">
                    <i class="fas fa-user-tie fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Propietarios
                            
                        </span>
                    </a>
                    
                </li>
                <li class="has-subnav">
                    <a href="ListadoResidentes.php?pagina=1">
                       <i class="fas fa-user-alt fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Residentes
                        </span>
                    </a>
                    
                </li>
                <li class="has-subnav">
                    <a href="ListadoPagos.php?pagina=1">
                       <i class="fas fa-file-invoice-dollar fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Pagos
                        </span>
                    </a>
                   
                </li>
                <li>
                    <a href="Informes.php?pagina=1">
                        <i class="fa fa-bar-chart-o fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Informes
                        </span>
                    </a>
                </li>
                
            </ul>

            <ul class="logout">
                <li>
                   <a href="../CerrarSesion.php">
                         <i class="fa fa-power-off fa-2x" id="iconos-menu"></i>
                        <span class="nav-text">
                            Cerrar sesión
                        </span>
                    </a>
                </li>  
            </ul>
        </nav>
  </header>
  <div id="body">
    <h1 align="center">CONSULTAR APARTAMENTO</h1>
    <br>
    <a class="boton" href="ListarApartamento.php"><-volver</a>
    <center>
    <form class="form-horizontal" action="../Controlador/ControladorApartamento.php" style="align-content: center" method="POST">
        
<div class="form-group">
        <label for="radio" class="control-label col-xs-4">Número de apartamento:</label>
        <div class="col-xs-2">
        <input class="form-control" readonly type="text" name="NApartamento" id="NApartamento" value="<?php echo $apartamento->getNApartamento();?>">
        </div>
</div>
 
        
      <input name="IdEstado" id="IdEstado" class="form-control" value="Activo" hidden>
    <div class="form-group">
       
        
        <label for="radio" class="control-label col-xs-4">Coeficiente de copropiedad:</label>
        <div class="col-xs-2">
            <input class="form-control" readonly type="text" name="MetrosCuadrados" id="MetrosCuadrados"value="<?php echo $apartamento->getMetrosCuadrados();?>">
        </div>
    
        <label for="radio" class="control-label col-xs-2">Cuota de administración:</label>
        <div class="col-xs-2">
            <input class="form-control" readonly type="text" name="ValorMetrosCuadrados" id="ValorMetrosCuadrados"value="<?php echo $apartamento->getValorMetrosCuadrados();?>">
        </div>
    </div>
    <div class="form-group">
    <label for="radio" class="control-label col-xs-4">Parqueadero:</label> 
    <div class="col-xs-2">
      <input class="form-control" readonly type="text" name="Parqueadero" id="Parqueadero" value="<?php echo $apartamento->getparqueadero();?>">
    </div>
        <label for="radio" class="control-label col-xs-2">Valor parqueadero:</label>
        <div class="col-xs-2">
            <input class="form-control" readonly type="text" name="ValorParqueadero" id="ValorParqueadero"value="<?php echo $apartamento->getValorParqueadero();?>">
        </div>
    </div>

    <div class="form-group">
  <label for="radio" class="control-label col-xs-4"><label style="color: red;" for="" id="ValidarIdentificadorP"></label>Identificador de parqueadero:</label> 
    <div class="col-xs-2">
      <input class="form-control" type="text" maxlength="10" name="IdParqueadero" readonly id="IdParqueadero" value="<?php echo $apartamento->getIdParqueadero();?>">
    </div>
  <label for="radio" class="control-label col-xs-2"><label style="color: red;" for="" id="ValidarIdentificadorC"></label>Identificador de cuarto útil:</label> 
    <div class="col-xs-2">
      <input class="form-control" type="text" maxlength="10" name="IdCuartoUtil"readonly id="IdCuartoUtil" value="<?php echo $apartamento->getIdCuartoUtil();?>">
    </div>
</div>

    <div class="form-group">
        
        <label for="radio" class="control-label col-xs-4">Cuarto útil:</label>
        <div class="col-xs-2">
            <input class="form-control" readonly name="CuartoUtil" id="CuartoUtil" value="<?php echo $apartamento->getCuartoUtil();?>">
        </div>

        <label for="radio" class="control-label col-xs-2">Valor cuarto útil:</label>
        <div class="col-xs-2">
            <input class="form-control" readonly type="text" name="ValorCuartoUtil" id="ValorCuartoUtil" value="<?php echo $apartamento->getValorCuartoUtil();?>">
        </div>
    </div>
    <center>
    <br><br><br>
        <button type="button" class="btn btn-info" onclick="VolverAp()">Volver</button>
    </center>
    </form><br>
    <footer class="border-top footer" style="font-family:fantasy">
        <div class="container">
            &copy; 2020 - S.T.A.B
        </div>
    </footer>
</center>
</body>
<script src="../js/funciones.js"></script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/acf5d1b9db.js" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
</html>