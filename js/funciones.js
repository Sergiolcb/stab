/*------------- VALIDACIONES PAGO ADELANTADO -------------------*/

$(document).ready(function() {
        $("#FrmPagos").submit(function(event) {
            event.preventDefault();
            let FormularioValidado = true;

            if ($("#NcuentaCobro").val().length == 0) {
                $("#ValidarNcuentaCobro").text("*");
                FormularioValidado = false;
            } else {
                $("#ValidarNcuentaCobro").text("");
            }

            if ($("#Periodo").val().length == 0) {
                $("#ValidarPeriodoInicio").text("*");
                FormularioValidado = false;
            } else {
                $("#ValidarPeriodoInicio").text("");
            }

            if ($("#PeriodoFin").val().length == 0) {
                $("#ValidarPeriodoFin").text("*");
                FormularioValidado = false;
            } else {
                $("#ValidarPeriodoFin").text("");
            }

            if ($("#FechaLimite").val().length == 0) {
                $("#ValidarFechaLimite").text("*");
                FormularioValidado = false;
            } else {
                $("#ValidarFechaLimite").text("");
            }
            if ($("#NApartamento").val() == 0) {
                $("#ValidarNapartamento").text("*");
                FormularioValidado = false;
            } else {
                $("#ValidarNapartamento").text("");
            }
            if ($("#TotalPagar").val().length == 0) {
                $("#ValidarPagoTotal").text("*");
                FormularioValidado = false;
            } else {
                $("#ValidarPagoTotal").text("");
            }

            if ($("#Propietario").val().length == 0) {
                $("#ValidarPropietario").text("*");
                FormularioValidado = false;
            } else {
                $("#ValidarPropietario").text("");
            }

            if ($("#M2").val() == 0) {
                $("#ValidarM2").text("*");
                FormularioValidado = false;
            } else {
                $("#ValidarM2").text("");
            }

            if ($("#Parqueadero").val().length == 0) {
                $("#ValidarParqueadero").text("*");
                FormularioValidado = false;
            } else {
                $("#ValidarParqueadero").text("");
            }

            if ($("#Cuarto").val().length == 0) {
                $("#ValidarCuartoutil").text("*");
                FormularioValidado = false;
            } else {
                $("#ValidarCuartoutil").text("");
            }

            if (FormularioValidado == false) {
                sweetAlert("Error", "Se debe corregir todos los campos marcados en rojo", "error");

            } else {
                var url = "../Controlador/ControladorPago.php";
                $.ajax({
                    type: "POST",
                    url: url,
                    data: $("#FrmPagos").serialize(),
                    success: function(data) {
                        $('#RespuestaTransaccion').html(data);
                    }
                });
            }

        })
    })
    /*----------------------------------------------Cuota de administracion--------------------------*/
$(document).ready(function() {
    $("#FrmCuota").submit(function(event) {
        event.preventDefault();
        let FormularioValidado = true;

        if ($("#NcuentaCobro").val().length == 0) {
            $("#ValidarNcuentaCobro").text("*");
            FormularioValidado = false;
        } else {
            $("#ValidarNcuentaCobro").text("");
        }

        if ($("#Periodo").val().length == 0) {
            $("#ValidarPeriodoInicio").text("*");
            FormularioValidado = false;
        } else {
            $("#ValidarPeriodoInicio").text("");
        }

        if ($("#FechaLimite").val().length == 0) {
            $("#ValidarFechaLimite").text("*");
            FormularioValidado = false;
        } else {
            $("#ValidarFechaLimite").text("");
        }
        if ($("#NApartamento").val() == 0) {
            $("#ValidarNapartamento").text("*");
            FormularioValidado = false;
        } else {
            $("#ValidarNapartamento").text("");
        }
        if ($("#TotalPagar").val().length == 0) {
            $("#ValidarPagoTotal").text("*");
            FormularioValidado = false;
        } else {
            $("#ValidarPagoTotal").text("");
        }

        if ($("#Propietario").val().length == 0) {
            $("#ValidarPropietario").text("*");
            FormularioValidado = false;
        } else {
            $("#ValidarPropietario").text("");
        }

        if ($("#M2").val() == 0) {
            $("#ValidarM2").text("*");
            FormularioValidado = false;
        } else {
            $("#ValidarM2").text("");
        }

        if ($("#Parqueadero").val().length == 0) {
            $("#ValidarParqueadero").text("*");
            FormularioValidado = false;
        } else {
            $("#ValidarParqueadero").text("");
        }

        if ($("#Cuarto").val().length == 0) {
            $("#ValidarCuartoutil").text("*");
            FormularioValidado = false;
        } else {
            $("#ValidarCuartoutil").text("");
        }

        if (FormularioValidado == false) {
            sweetAlert("Error", "Se debe corregir todos los campos marcados en rojo", "error");

        } else {
            var url = "../Controlador/ControladorCuota.php";
            $.ajax({
                type: "POST",
                url: url,
                data: $("#FrmCuota").serialize(),
                success: function(data) {
                    $('#RespuestaTransaccion').html(data);
                }
            });
        }

    })
})

/*******************************************************MULTA*************************** */

$(document).ready(function() {
        $("#FrmMulta").submit(function(event) {
            event.preventDefault();
            let FormularioValidado = true;

            if ($("#NcuentaCobro").val().length == 0) {
                $("#ValidarNcuentaCobro").text("*");
                FormularioValidado = false;
            } else {
                $("#ValidarNcuentaCobro").text("");
            }

            if ($("#Periodo").val().length == 0) {
                $("#ValidarPeriodoInicio").text("*");
                FormularioValidado = false;
            } else {
                $("#ValidarPeriodoInicio").text("");
            }

            if ($("#FechaLimite").val().length == 0) {
                $("#ValidarFechaLimite").text("*");
                FormularioValidado = false;
            } else {
                $("#ValidarFechaLimite").text("");
            }
            if ($("#NApartamentoMulta").val() == 0) {
                $("#ValidarNapartamento").text("*");
                FormularioValidado = false;
            } else {
                $("#ValidarNapartamento").text("");
            }
            if ($("#TotalaPagar").val().length == 0) {
                $("#ValidarPagoTotal").text("*");
                FormularioValidado = false;
            } else {
                $("#ValidarPagoTotal").text("");
            }

            if ($("#Propietario").val().length == 0) {
                $("#ValidarPropietario").text("*");
                FormularioValidado = false;
            } else {
                $("#ValidarPropietario").text("");
            }

            if ($("#ValorMulta").val() == 0) {
                $("#ValidarM2").text("*");
                FormularioValidado = false;
            } else {
                $("#ValidarM2").text("");
            }

            if (FormularioValidado == false) {
                sweetAlert("Error", "Se debe corregir todos los campos marcados en rojo", "error");

            } else {
                var url = "../Controlador/ControladorMulta.php";
                $.ajax({
                    type: "POST",
                    url: url,
                    data: $("#FrmMulta").serialize(),
                    success: function(data) {
                        $('#RespuestaTransaccion').html(data);
                    }
                });
            }

        })
    })
    /*-----------------Cargar fecha actual--------------
    var fecha = new Date();
      var mes = fecha.getMonth()+1;
      var dia = fecha.getDate();
      var ano = fecha.getFullYear();
      if(dia<10)
        dia='0'+dia;
      if(mes<10)
        mes='0'+mes;
      document.getElementById('FechaActual').value=dia+"/"+mes+"/"+ano;
    */


/*-----------------Validar fecha--------------*/

function validarFechas() {
    var fechainicial = document.getElementById('Periodo').value;
    var fechafinal = document.getElementById('PeriodoFin').value;


    if (fechafinal != "")
        if (fechafinal <= fechainicial) {

            sweetAlert("Error", "La fecha de el periodo fin no puede ser menor o igual al periodo inicio.", "error");
            document.getElementById('PeriodoFin').value = "";
            document.getElementById('TotalPagar').value = "";
        } else {

            numMeses = fechafinal[5] + fechafinal[6] - (fechainicial[5] + fechainicial[6]);
            numAnos= (fechafinal[0] + fechafinal[1] + fechafinal[2] + fechafinal[3] - (fechainicial[0] + fechainicial[1]+ fechainicial[2] + fechainicial[3]))*12;
            numMeses = (numMeses + 1)+numAnos;


            var Parqueadero = document.getElementById('Parqueadero').value;
            var Cuarto = document.getElementById('Cuarto').value;
            var M2 = document.getElementById('M2').value;

            TotalaPagar = numMeses * (Number(Parqueadero) + Number(Cuarto) + Number(M2))


            document.getElementById('TotalPagar').value = TotalaPagar;

        }

}

function totalpagar() {
    var Parqueadero = document.getElementById('Parqueadero').value;
    var Cuarto = document.getElementById('Cuarto').value;
    var M2 = document.getElementById('M2').value;
    var Multa = document.getElementById('ValorMulta').value;
    var SaldoaFavor = document.getElementById('SaldoaFavor').value;
    TotalaPagar = (Number(Multa) + Number(Parqueadero) + Number(Cuarto) + Number(M2) - Number(SaldoaFavor))
    console.log(Parqueadero, Cuarto, M2, Multa, SaldoaFavor);

    document.getElementById('TotalPagar').value = TotalaPagar;
}

/*************************************MULTA************************************************************ */
function totalpagar2() {
    var multa = document.getElementById('ValorMulta').value;

    TotalaPagar = Number(multa)


    document.getElementById('TotalaPagar').value = TotalaPagar;
}
/************************ FECHAS ******************************* */
$('.datetime').flatpickr({
    minDate: "today",
    enableTime: true,
    altInput: true,
    altFormat: "d-m-Y h:i:K",
    dateFormat: "Y-m-d H:i",
    locale: {
        weekdays: {
            shorthand: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
            longhand: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        },
        months: {
            shorthand: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Оct', 'Nov', 'Dic'],
            longhand: ['Enero', 'Febrero', 'Мarzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        },
    }
});

$('.date').flatpickr({
    defaultDate: "today",
    altInput: true,
    altFormat: "d-m-Y",
    dateFormat: "Y-m-d",
    noCalendar: true,

});

$('.datetimeconsulta').flatpickr({
    altInput: true,
    altFormat: "d-m-Y h:i:K",
    dateFormat: "Y-m-d H:i",
    noCalendar: true,
});

$('.dateconsulta').flatpickr({
    altInput: true,
    altFormat: "d-m-Y",
    dateFormat: "Y-m-d",
    noCalendar: true,

});



function carg() {
    var NApartamento = document.getElementById('NApartamento').value;
    console.log(NApartamento);
    if (NApartamento == 0)
    {
$('.calendarioI').flatpickr({
    minDate: "today",
    maxDate: "",
    defaultDate: "",
    altInput: true,
    altFormat: "F Y",
    dateFormat: "Y-m-d",
    noCalendar: true,
    locale: {
        weekdays: {
            shorthand: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
            longhand: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        },
        months: {
            shorthand: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Оct', 'Nov', 'Dic'],
            longhand: ['Enero', 'Febrero', 'Мarzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        },
    }
});
}
else
{
    $('.calendarioI').flatpickr({
        minDate: "today",
        maxDate: "",
        defaultDate: "",
        altInput: true,
        altFormat: "F Y",
        dateFormat: "Y-m-d",
        locale: {
            weekdays: {
                shorthand: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                longhand: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
            },
            months: {
                shorthand: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Оct', 'Nov', 'Dic'],
                longhand: ['Enero', 'Febrero', 'Мarzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            },
        }
    }); 
}
}

$('#NApartamento').bind('change', function() {
    $('.calendarioI').val("");
 });

$('.calendarioC').flatpickr({
    maxDate: "",
    defaultDate: "",
    altInput: true,
    altFormat: "F Y",
    dateFormat: "Y-m-d",
    locale: {
        weekdays: {
            shorthand: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
            longhand: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        },
        months: {
            shorthand: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Оct', 'Nov', 'Dic'],
            longhand: ['Enero', 'Febrero', 'Мarzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        },
    },
    noCalendar: true,
});

$('.calendarioy').flatpickr({
    minDate: "today",
    maxDate: "",
    defaultDate: "",
    altInput: true,
    altFormat: "F Y",
    dateFormat: "Y-m-d",
    locale: {
        weekdays: {
            shorthand: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
            longhand: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        },
        months: {
            shorthand: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Оct', 'Nov', 'Dic'],
            longhand: ['Enero', 'Febrero', 'Мarzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        },
    }
});

/*------------- VALIDACIONES APARTAMENTO -------------------*/
$(document).ready(function() {
    $("#FrmApartamentos").submit(function(event) {
        event.preventDefault();
        let FormularioValidado = true;

        if ($("#NApartamento").val() == 0) {
            $("#ValidarNApartamento").text("*");
            FormularioValidado = false;
        } else {
            $("#ValidarNApartamento").text("");
        }

        if ($("#MetrosCuadrados").val() == 0) {
            $("#ValidarMetrosCuadrados").text("*");
            FormularioValidado = false;
        } else {
            $("#ValidarMetrosCuadrados").text("");
        }

        if ($("#ValorMetrosCuadrados").val() == 0) {
            $("#ValidarValorMetrosCuadrados").text("*");
            FormularioValidado = false;
        } else {
            $("#ValidarValorMetrosCuadrados").text("");
        }
    
        if (FormularioValidado == false) {
           // sweetAlert("Error", "Se debe corregir todos los campos marcados en rojo", "error");

        } else {
            var url = "../Controlador/ControladorApartamento.php";
            $.ajax({
                type: "POST",
                url: url,
                data: $("#FrmApartamentos").serialize(),
                success: function(data) {
                    $('#RespuestaApartamento').html(data);
                    $('#RespuestaTransaccionApartamento').html(data);
                }
            });
        }

    })
})

/*------------- VALIDACIONES PROPIETARIO -------------------*/
$(document).ready(function() {
    $("#FrmPropietarios").submit(function(event) {
        event.preventDefault();
        let FormularioValidado = true;

        if ($("#Nombre").val().length == 0) {
            $("#ValidarNombre").text("*");
            FormularioValidado = false;
        } else {
            $("#ValidarNombre").text("");
        }

        if ($("#Cedula").val().length == 0) {
            $("#ValidarCedula").text("*");
            FormularioValidado = false;
        } else {
            $("#ValidarCedula").text("");
        }

        if ($("#NApartamento").val() == 0) {
            $("#ValidarNApartamento").text("*");
            FormularioValidado = false;
        } else {
            $("#ValidarNApartamento").text("");
        }

        if($("#Correo").val().indexOf('@', 0) == -1 || $("#Correo").val().indexOf('.', 0) == -1) {
            $("#ValidarCorreo").text("*");
            FormularioValidado = false;
             } else {
                $("#ValidarCorreo").text("");
               }
         
         if (FormularioValidado == false) {
            sweetAlert("Error", "Se debe corregir todos los campos marcados en rojo", "error");

        } else {
            var url = "../Controlador/ControladorPropietario.php";
            $.ajax({
                type: "POST",
                url: url,
                data: $("#FrmPropietarios").serialize(),
                success: function(data) {
                    $('#RespuestaTransaccionPropietario').html(data);
                }
            });
        }

    })
})
$('.selectpicker').selectpicker();

/*------------- VALIDACIONES RESIDENTES -------------------*/
$(document).ready(function() {
    $("#FrmResidentes").submit(function(event) {
        event.preventDefault();
        let FormularioValidado = true;

        if ($("#Nombre").val().length == 0) {
            $("#ValidarNombre").text("*");
            FormularioValidado = false;
        } else {
            $("#ValidarNombre").text("");
        }

        if ($("#Cedula").val().length == 0) {
            $("#ValidarCedula").text("*");
            FormularioValidado = false;
        } else {
            $("#ValidarCedula").text("");
        }

        if ($("#NApartamento").val() == 0) {
            $("#ValidarNApartamento").text("*");
            FormularioValidado = false;
        } else {
            $("#ValidarNApartamento").text("");
        }

        if($("#Correo").val().indexOf('@', 0) == -1 || $("#Correo").val().indexOf('.', 0) == -1) {
            $("#ValidarCorreo").text("*");
            FormularioValidado = false;
             } else {
                $("#ValidarCorreo").text("");
               }
        if (FormularioValidado == false) {
            sweetAlert("Error", "Se debe corregir todos los campos marcados en rojo", "error");

        } else {
            var url = "../Controlador/ControladorResidente.php";
            $.ajax({
                type: "POST",
                url: url,
                data: $("#FrmResidentes").serialize(),
                success: function(data) {
                    $('#RespuestaTransaccionResidente').html(data);
                }


            });
        }

    })
})

function Volver() {
    window.location.href = "ListadoResidentes.php";
}

function VolverP() {
    window.location.href = "ListadoPropietarios.php";
}

function VolverI() {
    window.location.href = "Informes.php?pagina=1";
}

/*------------------Cargar campos ocultos -----------------------------------*/

$(document).ready(function() {

    $("#show").on('click', function() {
        $("#Mostrarmulta").show();
        return false;
    });
});

$(document).ready(function() {

    $("#showSaldo").on('click', function() {
        $("#MostrarSaldoFavor").show();
        return false;
    });
});


function showContent() {
    element = document.getElementById("MostrarAbono");
    element2 = document.getElementById("MostrarPagoCompleto");
    Pago = document.getElementById("PagoCompleto");
    check = document.getElementById("Abono");
    if (check.checked) {
        element.style.display = 'block';
        element2.style.display = 'none';
    } else if (Pago.checked) {
        element2.style.display = 'block';
        element.style.display = 'none';
    }
}


/***********************************DATOS AUTOCOMPLETABLES************************************************ */
$(document).ready(function() {
    $('#NApartamento').on('change', function() {
        var NApartamento = $(this).val();
        if (NApartamento) {
            $.ajax({
                type: 'POST',
                url: '../Vistas/DatosAjax.php',
                data: 'NApartamento=' + NApartamento + '&Tipo=Nombre',
                success: function(html) {
                    $('#Propietario').val(html);
                }
            });
            $.ajax({
                type: 'POST',
                url: '../Vistas/DatosAjax.php',
                data: 'NApartamento=' + NApartamento + '&Tipo=Email',
                success: function(html) {
                    $('#Email').val(html);
                }
            });
            $.ajax({
                type: 'POST',
                url: '../Vistas/DatosAjax.php',
                data: 'NApartamento=' + NApartamento + '&Tipo=DireccionEntrega',
                success: function(html) {
                    $('#DireccionEntrega').val(html);
                }
            });

            $.ajax({
                type: 'POST',
                url: '../Vistas/DatosAjax.php',
                data: 'NApartamento=' + NApartamento + '&Tipo=Parqueadero',
                success: function(html) {
                    $('#Parqueadero').val(html);
                }
            });

            $.ajax({
                type: 'POST',
                url: '../Vistas/DatosAjax.php',
                data: 'NApartamento=' + NApartamento + '&Tipo=M2',
                success: function(html) {
                    $('#M2').val(html);
                }
            });

            $.ajax({
                type: 'POST',
                url: '../Vistas/DatosAjax.php',
                data: 'NApartamento=' + NApartamento + '&Tipo=Cuarto',
                success: function(html) {
                    $('#Cuarto').val(html);
                }
            });

            $.ajax({
                type: 'POST',
                url: '../Vistas/DatosAjax.php',
                data: 'NApartamento=' + NApartamento + '&Tipo=TotalPagar',
                success: function(html) {
                    $('#TotalPagar').val(html);
                }
            });

            $.ajax({
                type: 'POST',
                url: '../Vistas/DatosAjax.php',
                data: 'NApartamento=' + NApartamento + '&Tipo=TotalaPagar',
                success: function(html) {
                    $('#TotalaPagar').val(html);
                }
            });
        }
    });
});

/***********************************DATOS AUTOCOMPLETABLES MULTA************************************************ */
$(document).ready(function() {
    $('#NApartamentoMulta').on('change', function() {
        var NApartamento = $(this).val();
        if (NApartamento) {
            $.ajax({
                type: 'POST',
                url: '../Vistas/DatosAjaxMulta.php',
                data: 'NApartamento=' + NApartamento + '&Tipo=Nombre',
                success: function(html) {
                    $('#Propietario').val(html);
                }
            });
            $.ajax({
                type: 'POST',
                url: '../Vistas/DatosAjaxMulta.php',
                data: 'NApartamento=' + NApartamento + '&Tipo=Email',
                success: function(html) {
                    $('#Email').val(html);
                }
            });
            $.ajax({
                type: 'POST',
                url: '../Vistas/DatosAjaxMulta.php',
                data: 'NApartamento=' + NApartamento + '&Tipo=DireccionEntrega',
                success: function(html) {
                    $('#DireccionEntrega').val(html);
                }
            });

            $.ajax({
                type: 'POST',
                url: '../Vistas/DatosAjaxMulta.php',
                data: 'NApartamento=' + NApartamento + '&Tipo=Parqueadero',
                success: function(html) {
                    $('#Parqueadero').val(html);
                }
            });

            $.ajax({
                type: 'POST',
                url: '../Vistas/DatosAjaxMulta.php',
                data: 'NApartamento=' + NApartamento + '&Tipo=M2',
                success: function(html) {
                    $('#M2').val(html);
                }
            });

            $.ajax({
                type: 'POST',
                url: '../Vistas/DatosAjaxMulta.php',
                data: 'NApartamento=' + NApartamento + '&Tipo=Cuarto',
                success: function(html) {
                    $('#Cuarto').val(html);
                }
            });

            $.ajax({
                type: 'POST',
                url: '../Vistas/DatosAjaxMulta.php',
                data: 'NApartamento=' + NApartamento + '&Tipo=TotalPagar',
                success: function(html) {
                    $('#TotalPagar').val(html);
                }
            });

            $.ajax({
                type: 'POST',
                url: '../Vistas/DatosAjaxMulta.php',
                data: 'NApartamento=' + NApartamento + '&Tipo=TotalaPagar',
                success: function(html) {
                    $('#TotalaPagar').val(html);
                }
            });

            $.ajax({
                type: 'POST',
                url: '../Vistas/DatosAjaxMulta.php',
                data: 'NApartamento=' + NApartamento + '&Tipo=Residente',
                success: function(html) {
                    $('#Residente').val(html);
                }
            });
        }
    });
});

/***********************************************Abonos*************************** */
$('#dialogo2').on('show.bs.modal', function(event) {
    var button = $(event.relatedTarget) // Botón que activó el modal
    var NCuentaCobro = button.data('ncuentacobro') // Extraer la información de atributos de datos
    var PagoCompleto = button.data('pagocompleto') // Extraer la información de atributos de datos

    var modal = $(this)
    modal.find('.modal-body #ncuentacobro').val(NCuentaCobro)
    modal.find('.modal-body #pagocompleto').val(PagoCompleto)

    $('.alert').hide(); //Oculto alert
})

function CambiodeEstado() {

    var Abono = document.getElementById("abono").value;
    var PagoCompleto = document.getElementById("pagocompleto").value;
    if (Abono < PagoCompleto) {
        document.getElementById('estado').value = 4;
    } else if (Abono == PagoCompleto) {
        document.getElementById('estado').value = 5;
        console.log(PagoCompleto)
    }
}


$(document).ready(function() {
    $("#frmAbono").submit(function(event) {
        event.preventDefault();
        let FormularioValidado = true;
        let Abono = $('#abono').val() * 1;
        let PagoCompleto = $('#pagocompleto').val() * 1;

        if ($("#abono").val().length == 0) {
            $("#ValidarAbono").text("*");
            FormularioValidado = false;
        } else {
            $("#ValidarAbono").text("");
        }
        if (Abono > PagoCompleto) {
            sweetAlert("Error", "El valor ingresado es superior al valor total a pagar", "error");
        } else {
            if (FormularioValidado == false) {
                sweetAlert("Error", "Se debe corregir todos los campos marcados en rojo", "error");

            } else {
                var url = "../Controlador/ControladorPago.php";
                $.ajax({
                    type: "POST",
                    url: url,
                    data: $("#frmAbono").serialize(),
                    success: function(data) {
                        $('#RespuestaTransaccionAbono').html(data);
                    }
                });
            }

        }
        console.log(PagoCompleto)
        console.log(Abono)


    })
})

$("#dialogo2").on("hidden.bs.modal", function() { $(".modal-body input").val(""); });


/********************Buscador****************************************** */
$(document).ready(function() {
    $('#entradafilter').keyup(function() {
        var rex = new RegExp($(this).val(), 'i');
        $('.contenidobusqueda tr').hide();
        $('.contenidobusqueda tr').filter(function() {
            return rex.test($(this).text());
        }).show();

    })

});
/******************************************Validar campos numericos */
$(document).ready(function() {
    $("#Celular").on("keydown", function() {
        if (event.key.length < 2) {
            var n = parseInt(this.value + event.key);
            if (isNaN(event.key) || n > this.maxlength)
                return false;
        };
    });
});

$(document).ready(function() {
    $("#Telefono").on("keydown", function() {
        if (event.key.length < 2) {
            var n = parseInt(this.value + event.key);
            if (isNaN(event.key) || n > this.maxlength)
                return false;
        };
    });
});

$(document).ready(function() {
    $("#ValorMetrosCuadrados").on("keydown", function() {
        if (event.key.length < 2) {
            var n = parseInt(this.value + event.key);
            if (isNaN(event.key) || n > this.maxlength)
                return false;
        };
    });
});

$(document).ready(function() {
    $("#ValorParqueadero").on("keydown", function() {
        if (event.key.length < 2) {
            var n = parseInt(this.value + event.key);
            if (isNaN(event.key) || n > this.maxlength)
                return false;
        };
    });
});

$(document).ready(function() {
    $("#ValorCuartoUtil").on("keydown", function() {
        if (event.key.length < 2) {
            var n = parseInt(this.value + event.key);
            if (isNaN(event.key) || n > this.maxlength)
                return false;
        };
    });
});

$(document).ready(function() {
    $("#NApartamento").on("keydown", function() {
        if (event.key.length < 2) {
            var n = parseInt(this.value + event.key);
            if (isNaN(event.key) || n > this.maxlength)
                return false;
        };
    });
});
/** FUNCIONES APARTAMENTO */

function validar()
  {
    var selectParque = document.getElementById('Parqueadero');
    var selectCuarto = document.getElementById('CuartoUtil');
    selectParque.addEventListener('change',
      function(){
        var selectedOptionParque = this.options[selectParque.selectedIndex];
        var selectedOptionCuarto = this.options[selectCuarto.selectedIndex];
        if (selectedOptionParque.value == 1) {
            if (selectedOptionCuarto.value == 'Sí') {
                document.getElementById('ValorParqueadero').disabled = false;
                document.getElementById('ValorCuartoUtil').disabled = false;
                document.getElementById('CuartoUtil').value='No';
            }else{
                document.getElementById('ValorParqueadero').disabled = false;
                document.getElementById('ValorCuartoUtil').disabled = true;
                document.getElementById('CuartoUtil').value='No';
            }
        }else if (selectedOptionParque.value == 2){
            if (selectedOptionCuarto.value == 'Sí') {
                document.getElementById('ValorCuartoUtil').disabled = false;
                document.getElementById('ValorParqueadero').disabled = false;
            }else{
                document.getElementById('ValorCuartoUtil').disabled = false;
                document.getElementById('CuartoUtil').value='Sí';
                document.getElementById('ValorParqueadero').disabled = false;
            }
        }else{
            document.getElementById('ValorParqueadero').disabled = true;
            document.getElementById('CuartoUtil').value='No';
            document.getElementById('Parqueadero').value=0;
            document.getElementById('ValorCuartoUtil').disabled = true;
        }
      });
      selectCuarto.addEventListener('change',
        function(){
            var selectedOptionCuarto = this.options[selectCuarto.selectedIndex];
            if (selectedOptionCuarto.value == 'No'){
                document.getElementById('ValorCuartoUtil').disabled = true;
                document.getElementById('CuartoUtil').value='No';
            }else if(selectedOptionCuarto.value == 'Sí'){
                document.getElementById('ValorCuartoUtil').disabled = false;
                document.getElementById('CuartoUtil').value='Sí';
            }else{
                document.getElementById('ValorCuartoUtil').disabled = true;
                document.getElementById('CuartoUtil').value='No';
            }
        });
  }

function VolverAp() {
    window.location.href="ListarApartamento.php";
} 